import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import './index.css';
import App from './App/App';
import registerServiceWorker from './registerServiceWorker';
import store from './App/store';
import deepForceUpdate from 'react-deep-force-update';

let wholeApp;

const forceRefresh = function forceRefresh() {
  deepForceUpdate(wholeApp);
};

console.log(store.getState());
wholeApp = ReactDOM.render(
  <Provider store={store}>
    <App forceRefresh={forceRefresh}/>
  </Provider>, 
document.getElementById('root'));
registerServiceWorker();
