import React from 'react';
import PropTypes from 'prop-types';

export default class Toggle extends React.PureComponent {
  static propTypes = {
    isItDown: PropTypes.bool,
    toggleDown: PropTypes.func.isRequired,
    toggleUp: PropTypes.func.isRequired,
  }
  static defaultProps = {
    isItDown: false,
  }
  render() {
    const {
      isItDown,
      toggleDown,
      toggleUp
    } = this.props;
    if (!isItDown) {
      return (
        <button type="button" className="btn btn-default btn-sm" onClick={toggleDown}>
          <i className="fa fa-angle-double-down" />
        </button>
      )
    }
    return (
      <button type="button" className="btn btn-default btn-sm"  onClick={toggleUp}>
        <i className="fa fa-angle-double-up" />
      </button>
    )
  }
}