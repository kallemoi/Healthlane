import { combineReducers } from 'redux';
import PropTypes from 'prop-types';

const shapeOfMessage = PropTypes.shape({
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  sender: PropTypes.string.isRequired,
  timestamp: PropTypes.string.isRequired,
})

const initialMessagesState = [];

const messagesReducer = (state = initialMessagesState, action) => {
  switch (action.type) {
    case 'SET_MESSAGES':
      return action.payload.messages;
    case 'ADD_MESSAGE':
      return [...state, action.payload.message]
    default:
      return state
  }
}

export default combineReducers({
  messages: messagesReducer,
});
