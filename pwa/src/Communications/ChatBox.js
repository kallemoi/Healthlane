import { connect } from 'react-redux';
import ChatBoxView from './ChatBoxView.jsx';
import { addMessage } from './communicationActions';
import moment from 'moment';

const mapStateToProps = (state) => ({
  messages: state.communication.messages,
  currentUser: 'testiuseri',
});

const mapDispatchToProps = (dispatch) => {
  return {
    sendMessage: (text) => dispatch(addMessage({
      sender: 'testiuseri',
      text,
      id: (new Date()).getTime(),
      timestamp: moment().toISOString()
    }))
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ChatBoxView);
