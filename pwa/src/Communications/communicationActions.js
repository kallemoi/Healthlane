export const ADD_MESSAGE = 'ADD_MESSAGE';
export const SET_MESSAGES = 'SET_MESSAGES';

export const setMessages = (messages) => ({
  type: SET_MESSAGES,
  payload: {
    messages
  }
})

export const addMessage = (message) => ({ // shape of message
  type: ADD_MESSAGE,
  payload: {
    message
  }
});
