import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import now from '../App/timeMaster';

export default class ChatBoxView extends React.Component {
  static propTypes = {
    messages: PropTypes.array.isRequired,
    currentUser: PropTypes.string.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
    }
    this.onInputChange = this.onInputChange.bind(this);
  }
  onInputChange(e) {
    this.setState({
      textInput: e.target.value
    });
  }
  render() {
    const {
      messages,
      currentUser,
      sendMessage,
    } = this.props;
    const {
      textInput
    } = this.state;
    const me = currentUser;
    return (
      <div className="row">
        <div className="col-xs-12">
          <h4>Communication with healthcare</h4>
          <div className="well well-sm">
          {messages.map(msg => (
            <p key={msg.id} className={msg.sender === me ? "text-left" : "text-right"}>
              {msg.text} ({moment(msg.timestamp).from(now())})
            </p>
          ))}
          </div>
        </div>
        <div className="col-xs-12">
          <div className="form-inline">
            <div className="form-group">
              <label className="sr-only" htmlFor="inputMessageBox">Message</label>
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  id="inputMessageBox"
                  placeholder="type a message..."
                  value={textInput}
                  onChange={this.onInputChange}
                />
              </div>
            </div>
            <button 
              type="text"
              className="btn btn-primary"
              onClick={() => {
                sendMessage(textInput);
                this.setState({
                  textInput: ''
                });
              }}
            >
              Send message
            </button>
          </div>
        </div>
      </div>
    )
  }
}