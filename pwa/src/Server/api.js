import axios from 'axios';
import Promise from 'bluebird';
import store from '../App/store';
import {
  initialUserState
} from './devstate';

export default function api() {
  const base = store.getState().settings.apiBase;

  return {
    fetchEncounters: (patientId) =>
      axios.get(`${base}/Encountermock/${patientId}`),
    fetchMessages: (encounterId) =>
      axios.get(`${base}/Message`),
    fetchUser: (patientId) =>
      axios.get(`${base}/Patient/${patientId}`),
    saveUser: (userContact) => {
      const model = {};
      model.identifier = 300;
      model.street = "40 Great Plain Ave";
      model.zipcode = "60280";
      model.city = "Chicago";
      model.telecom = "123456"
      axios.post(`${base}/Patient/`, userContact)
    }
  }
}
