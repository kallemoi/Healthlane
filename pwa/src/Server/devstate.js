export const initialEncountersState = [
  {
    startDate: "2018-05-30T12:00:00Z",
    endDate: "2018-05-30T12:30:00Z",
    description: "Outpatient visit",
    status: "finished",
  }, {
    startDate: "2018-06-16T15:00:00Z",
    description: "Outpatient visit",
    status: "planned",
  }, {
    startDate: "2018-06-21T15:00:00Z",
    description: "Outpatient visit",
    status: "planned",
}];

export const initialMessagesState = [{
  id: 1,
  text: "Yo, I'm feeling ill",
  sender: "testiuseri",
  timestamp: "2018-06-10T10:00:00Z",
}, {
  id: 2,
  text: "Sup, take a chill pill",
  sender: "testprofessional",
  timestamp: "2018-06-10T10:12:00Z"
}, {
  id: 3,
  text: "Helped immediately! Thanks! :)",
  sender: "testiuseri",
  timestamp: "2018-06-10T10:45:00Z"
}];

export const initialUserState = {
  name: "Teppo Testi",
  userId: "testiuser",
  status: '', // home, intransit, athospital, waiting, appointment
  trustedUsers: [
    "testiparent"
  ],
  contact: {
    telecom: "+1444123456",
    street: "Testdrive avenue 214",
    city: "Maddog City",
    zipcode: "12345"
  }
}