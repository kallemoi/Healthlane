import axios from 'axios';

const loadSettings = function loadSettings() {
  return axios.get('settings.json');
}

export default loadSettings;