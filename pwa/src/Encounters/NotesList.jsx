import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class NotesList extends React.Component {
  static propTypes = {
    notes: PropTypes.array
  }
  static defaultProps = {
    notes: []
  }
  render() {
    const {
      notes
    } = this.props;
    if (notes.length === 0) return null;
    return (
      <div className="row">
        {notes.map(note => (
          <div key={note.id} className="col-xs-12">
            ({moment(note.date).format('LLL')}) {note.text}
          </div>
        ))}
      </div>
    );
  }
}