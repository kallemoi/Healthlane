import React from 'react';
import PropTypes from 'prop-types';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import now from '../App/timeMaster';

export default class AddNote extends React.Component {
  static propTypes = {
    saveNote: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      textInput: ''
    }
    this.onInputChange = this.onInputChange.bind(this);
  }
  onInputChange(e) {
    this.setState({
      textInput: e.target.value
    })
  }
  render() {
    const {
      saveNote
    } = this.props;
    const {
      textInput
    } = this.state;
    const sendMessage = (t) => console.log(t)
    return (
      <div className="row">
        {this.state.open && [
          <div className="col-xs-3">
            <DayPickerInput
              inputProps={{ className: "form-control" }}
              onDayChange={day => console.log(day)}
            />
          </div>,
          <div className="col-xs-9">
            <input
              type="text"
              className="form-control"
              id="inputMessageBox"
              placeholder="type a note..."
              value={textInput}
              onChange={this.onInputChange}
            />
          </div>
        ]}
        <div className="col-xs-12">
          {this.state.open ? (
            <button 
              type="text"
              className="btn btn-primary"
              onClick={() => {
                saveNote({
                  id: (new Date()).getTime(),
                  date: now().toISOString(),
                  text: textInput});
                this.setState({ open: false })
              }}
            >
              Save notes
            </button>
          )
          :
          (
            <button 
              type="text"
              className="btn btn-primary"
              onClick={() => this.setState({open: true})}
            >
              Add a note
            </button>
          )
          }
        </div>
      </div>
    );
  }
}