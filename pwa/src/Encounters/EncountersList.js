import { connect } from 'react-redux';
import EncountersListView from './EncountersListView';

const mapStateToProps = (state, ownProps) => {
  const { statuses } = ownProps; 
  return {
    encounters: state.encounters.filter(x => statuses.indexOf(x.status) !== -1)
  };
}

const dispatchToProps = (dispatch) => {
  return {};
}

export default connect(mapStateToProps)(EncountersListView);