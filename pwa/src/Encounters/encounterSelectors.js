import { createSelector } from 'reselect'

const passiveStates = ['finished', 'cancelled'];

export const allActiveEncounters = state => state.encounters.filter(x =>
  passiveStates.indexOf(x.status) === -1);

export const currentEncounter = createSelector(
  allActiveEncounters,
  encounters => {
    console.log(encounters);
    if (encounters.length == 0) return null;
    encounters.sort(x => x.startDate);
    return encounters[0];
  }
)

export default currentEncounter;