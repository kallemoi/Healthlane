import React from 'react';
import moment from 'moment';
import now from '../App/timeMaster';
import AnamnesisFormView from './AnamnesisFormView';
import ChatBox from '../Communications/ChatBox';
import Toggle from '../Shared/Toggle';
import GuideMap from '../Maps/GuideMap';
import AreaMap from '../Maps/AreaMap';
import ContactInformationForm from '../Users/ContactInformationForm';
import NotesList from './NotesList.jsx';
import AddNote from './AddNote.jsx';

const showDate = (isostring) =>
  isostring ? moment(isostring).format('LLLL') : '';

const headerForBooked = (encounter) => (
  <div>
    {moment(encounter.startDate).from(now())}
  </div>
);

const taskCompleteClass = (complete) => complete ? "alert alert-success" : "alert alert-warning"; 

export default class EncounterInfoView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showAnamnesis: false,
      showAreaMap: false,
      showHospitalMap: false,
      showContactInfo: false,
      isWaiting: false,
      calledIn: false,
    }
  }
  render() {
    const {
      encounter,
      user,
      saveNote,
      saveUserContact,
      saveQuestionnaire,
    } = this.props;
    if (!encounter) return null;
    const {
      showAnamnesis,
      showAreaMap,
      showHospitalMap,
      showContactInfo,
      isWaiting,
      calledIn
    } = this.state;
    const isToday = moment(encounter.startDate).isSame(now(), 'day');
    return (
      <div className="row">
        <div className="col-xs-12">
          {showDate(encounter.startDate)} - {showDate(encounter.endDate)}<br />
          {encounter.description} ({encounter.status})
        </div>
        <div className="col-xs-12">
          <h5>Prerequirements</h5>
          <div className={taskCompleteClass(encounter.tasks.contactInformationDone)} role="alert">
            1. Up-to-date contact information
            <Toggle
              isItDown={showContactInfo}
              toggleDown={() => this.setState({showContactInfo: true})}
              toggleUp={() => this.setState({showContactInfo: false})}
            />
            {showContactInfo &&
              <ContactInformationForm user={user} {...user.contact} saveUserContact={saveUserContact} />
            }
          </div>
          <div className={taskCompleteClass(encounter.tasks.questionnaireDone)} role="alert">
            2. Fill-in questionnaire&nbsp;&nbsp;
            <Toggle
              isItDown={showAnamnesis}
              toggleDown={() => this.setState({showAnamnesis: true})}
              toggleUp={() => this.setState({showAnamnesis: false})}
            />
          </div>
          {showAnamnesis &&
            <AnamnesisFormView saveQuestionnaire={() => saveQuestionnaire(encounter.startDate)} />
          }
          <div className={taskCompleteClass(true)} role="alert">
            3. Laboratory visit {moment("2018-06-05T05:00:00Z").format('LLLL')}&nbsp;&nbsp;
          </div>
          
        </div>
        <div className="col-xs-12">
          <h5>Notes and medications</h5>
          <NotesList notes={encounter.notes}/>
          {(encounter.status ==='planned') &&
            <AddNote saveNote={(note) => saveNote(note, encounter.startDate)} />
          }
        </div>
        {(isToday && !calledIn) &&
          <div className="col-xs-12">
            <h5>Area map</h5>
            <Toggle
              isItDown={showAreaMap}
              toggleDown={() => this.setState({showAreaMap: true})}
              toggleUp={() => this.setState({showAreaMap: false})}
            />
            {showAreaMap && 
              <AreaMap />
            }
          </div>
        }
        {(isToday && !calledIn) &&
          <div className="col-xs-12">
            <h5>Hospital map</h5>
            <Toggle
              isItDown={showHospitalMap}
              toggleDown={() => this.setState({showHospitalMap: true})}
              toggleUp={() => this.setState({showHospitalMap: false})}
            />
            {showHospitalMap && [
              <GuideMap mapName="tellus_entrance_a.png" />,
              <button type="button" className="btn btn-success" onClick={() => this.setState({calledIn: true, showHospitalMap: false, showAreaMap: false})}>
                Sign in when at the waiting area
              </button>,
              <p>
                Expected waiting time {now().from(moment(encounter.startDate), true)}
              </p>
            ]}
          </div>
        }
        {isWaiting &&
          <GuideMap mapName="tellus_waiting.png" />
        }
        {calledIn && [
          <h3 style={{ color: 'green' }}>You have been called in!</h3>,
          <GuideMap mapName="tellus_callin_15.png" />
        ]}
        {moment(encounter.startDate).isBefore(now().add(-2, 'hours')) &&
          <div className="col-xs-12">
            <ChatBox />
          </div>
        }
      </div>
    )
  }
}