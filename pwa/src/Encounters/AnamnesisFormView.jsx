import React from 'react';
import { toast } from 'react-toastify';

const saveToast = () => {
  toast("Saved!", {
    type: toast.TYPE.SUCCESS
  });
};

export default class AnamnesisFormView extends React.PureComponent {
  render() {
    const { saveQuestionnaire } = this.props;
    return (
      <div className="row">
        <div className="col-sm-12">
          <div className="form-horizontal">
            <div className="form-group">
              <label htmlFor="inputWeight" className="col-sm-2 control-label">Weight</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputWeight" placeholder="" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputTemperature" className="col-sm-2 control-label">Temperature</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputTemperature" placeholder="" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputPulse" className="col-sm-2 control-label">Heart rate</label>
              <div className="col-sm-10">
                <input type="number" className="form-control" id="inputPulse" placeholder="" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputBloodPressure" className="col-sm-2 control-label">Blood pressure</label>
              <div className="col-sm-10">
                <input type="text" className="form-control" id="inputBloodePressure" placeholder="e.g. 130/75" />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={() => {
                    saveToast();
                    saveQuestionnaire();
                  }}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}