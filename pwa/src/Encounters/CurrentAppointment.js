import { connect } from 'react-redux';
import EncounterInfoView from './EncounterInfoView.jsx';
import { currentEncounter } from './encounterSelectors';
import { getCurrentUser } from '../Users/userSelectors';
import { saveUserContact } from '../Users/userActions';
import { addNote, setQuestionnaireDone } from './encountersActions';


const mapStateToProps = (state) => {
  const encounter = currentEncounter(state);
  const user = getCurrentUser(state);
  if (!encounter) return {};
  encounter.tasks = {
    contactInformationDone: user.contact && user.contact.telecom && user.contact.street,
    questionnaireDone: (encounter && encounter.tasks && encounter.tasks.questionnaireDone) || false
  }
  return {
    user,
    encounter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveNote: (note, encounterStartDate) => dispatch(addNote(note, encounterStartDate)),
    saveUserContact: (userContact) => dispatch(saveUserContact(userContact)),
    saveQuestionnaire: (startDate) => dispatch(setQuestionnaireDone(startDate))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EncounterInfoView);