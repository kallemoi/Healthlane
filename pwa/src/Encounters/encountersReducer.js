import PropTypes from 'prop-types';
import {
  SET_ENCOUNTERS,
  SET_QUESTIONNAIRE_DONE,
  ADD_NOTE
} from './encountersActions.js'

const shapeOfNote = PropTypes.shape({
  id: PropTypes.number.isRequired,
  date: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
})

const shapeOfEncounter = PropTypes.shape({
  startDate: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  messages: PropTypes.array,
  notes: PropTypes.array
});

// planned | arrived | in-progress | onleave | finished | cancelled
const initialState = [];

const encounters = (state = initialState, action) => {
  switch (action.type) {
    case SET_ENCOUNTERS:
      return action.payload.encounters;
    case SET_QUESTIONNAIRE_DONE: {
      const encounter = state.find(x => x.startDate === action.payload.startDate);
      const otherEncounters = state.filter(x => x.startDate !== action.payload.startDate);
      const updatedEncounter = {...encounter};
      updatedEncounter.tasks = encounter.tasks || {};
      updatedEncounter.tasks.questionnaireDone = true;
      const newList = [...otherEncounters, updatedEncounter];
      newList.sort((a,b) => a.startDate > b.startDate);
      return newList;
    }
    case ADD_NOTE: {
      const encounter = state.find(x => x.startDate === action.payload.encounterStartDate);
      if (!encounter) return state;
      const otherEncounters = state.filter(x => x.startDate !== action.payload.encounterStartDate);
      const updatedEncounter = {...encounter};
      updatedEncounter.notes = encounter.notes ? [...encounter.notes, action.payload.note] : [action.payload.note];
      const newList = [...otherEncounters, updatedEncounter];
      newList.sort((a,b) => a.startDate > b.startDate);
      return newList;
    }
    default:
      return state
  }
}

export default encounters;