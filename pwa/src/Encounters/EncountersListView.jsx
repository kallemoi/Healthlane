import React from 'react';
import moment from 'moment';

export default class EncountersListView extends React.PureComponent {
  render() {
    const { encounters } = this.props;
    const encountersWithMoment = encounters.map(x => ({
      ...x,
      startDate: moment(x.startDate),
      endDate: x.endDate ? moment(x.endDate) : null
    }));
    return (
      <ul className="list-group">
        {encountersWithMoment.map(enc => (
          <li key={enc.startDate} className="list-group-item encounter" >
            {enc.startDate.format('LLLL')} {enc.description} {enc.status}&nbsp;&nbsp;
            {enc.status==='finished' &&
              <i className="fa fa-check" style={{ color: 'green' }} />
            }
            {enc.status==='cancelled' &&
              <i className="fa fa-calendar-times" style={{ color: 'red' }} />
            }
            {enc.status==='planned' &&
              <i className="fa fa-clock" style={{ color: 'cornflowerblue' }} />
            }
          </li>
        ))}
      </ul>
    )
  }
}