export const SET_ENCOUNTERS = 'SET_ENCOUNTERS';
export const SET_QUESTIONNAIRE_DONE = 'SET_QUESTIONNAIRE_DONE';
export const ADD_NOTE = 'ADD_NOTE';

export const setEncounters = function(encounters) {
  return {
    type: SET_ENCOUNTERS,
    payload: {
      encounters
    }
  }
}

export const setQuestionnaireDone = function(startDate) { // startdate used as id
  return {
    type: SET_QUESTIONNAIRE_DONE,
    payload: {
      startDate
    }
  }
}

export const addNote = function(note, encounterStartDate) {
  return {
    type: ADD_NOTE,
    payload: {
      note,
      encounterStartDate
    }
  }
}