import { SET_VIEW } from './viewStateActions';

const initialState = {
  view: "Home"
}

const viewState = (state = initialState, action) => {
  switch (action.type) {
    case (SET_VIEW): {
      return {
        view: action.payload.view
      }
    }
    default: 
    return state;
  }
}

export default viewState;
