import { combineReducers } from 'redux';
import encounters from '../Encounters/encountersReducer';
import communication from '../Communications/communicationReducer';
import user from '../Users/userReducer';
import users from '../Users/usersReducer';
import viewState from './viewStateReducer';
import settings from './settingsReducer';

export default combineReducers({
  settings,
  encounters,
  communication,
  user,
  users,
  viewState,
});
