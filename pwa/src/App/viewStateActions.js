export const SET_VIEW = 'SET_VIEW';

export const setHomeView = () => ({
  type: SET_VIEW,
  payload: {
    view: "Home"
  }
});

export const setUserView = () => ({
  type: SET_VIEW,
  payload: {
    view: "User"
  }
})

export const setPersonsOfInterestView = () => ({
  type: SET_VIEW,
  payload: {
    view: "PersonsOfInterest"
  }
})