import { SET_SETTINGS } from './appActions';

const initialSettingsState = {
  apiBase: '',
  userId: 300
}

const settings = (state = initialSettingsState, action) => {
  switch (action.type) {
    case SET_SETTINGS: 
      return {
        ...action.payload
      }
    default: 
      return state;
  }
}

export default settings;
