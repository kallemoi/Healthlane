import React from 'react';
import EncountersList from '../Encounters/EncountersList';
import CurrentAppointment from '../Encounters/CurrentAppointment';

export default class HomeView extends React.Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-12">
          <h4><u>Finished appointments</u></h4>
          <EncountersList statuses={['finished']} />

          <h4><u>Future appointments</u></h4>          
          <EncountersList statuses={['planned']} />

          <h4><u>Current appointment</u></h4>
          <div className="col-xs-12">
            <CurrentAppointment />
          </div>
        </div>
      </div>
    );
  }
}