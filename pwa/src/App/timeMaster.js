import moment from 'moment';
const offset = {
  value: 0,
  type: 'hours'
}

export const setOffsetHours = function setOffsetHours(hours) {
  offset.value = hours;
}

export default function now() {
  return moment().add(offset.value, offset.type);
}