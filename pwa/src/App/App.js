import { connect } from 'react-redux';
import AppView from './AppView.jsx';
import { getCurrentUser } from '../Users/userSelectors';
import { initializeApp } from './appActions';
import { setHomeView, setUserView, setPersonsOfInterestView } from './viewStateActions';

const mapStateToProps = (state) => {
  return {
    user: getCurrentUser(state),
    viewState: state.viewState,
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    initialize: () => dispatch(initializeApp()),
    setHome: () => dispatch(setHomeView()),
    setUser: () => dispatch(setUserView()),
    setPersonsOfInterest: () => dispatch(setPersonsOfInterestView()),
    forceRefresh: ownProps.forceRefresh
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppView);