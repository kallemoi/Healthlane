import React from 'react';
import PropTypes from 'prop-types';
import { ToastContainer, toast } from 'react-toastify';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import HomeView from './HomeView.jsx';
import UserSettings from '../Users/UserSettings';
import UsersOfInterestPage from '../Users/UsersOfInterestPage';
import TimeWalk from './TimeWalk.jsx';
import Hearbeat from './Heartbeat.jsx';

const notify = () => {
  toast("HA", {
    type: toast.TYPE.INFO
  });
};

export default class AppView extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
  }
  componentDidMount() {
    this.props.initialize();
  }
  render() {
    const {
      user,
      setHome,
      setUser,
      setPersonsOfInterest,
      viewState,
      forceRefresh
    } = this.props;
    return (
      <div className="App">
        <TimeWalk forceRefresh={forceRefresh} />
        <div className="bg-info header clearfix">
          <nav>
            <ul className="nav nav-pills pull-right">
              <li 
                onClick={setHome}
                role="presentation"
                className={viewState.view === 'Home' ? "active" : ""}
              >
                <a href="#"><i className="fa fa-home"/></a>
              </li>
              <li
                role="presentation"
                className={viewState.view === 'PersonsOfInterest' ? "active" : ""}
              >
              <a onClick={setPersonsOfInterest} href="#"><i className="fa fa-users"/></a>
              </li>
              <li
                role="presentation"
                className={viewState.view === 'User' ? "active" : ""}
              >
                <a onClick={setUser} href="#">{user.name} <i className="fa fa-user"/></a>
              </li>
            </ul>
          </nav>
          <h4 className="pull-left" style={{ color: '#337ab7', marginLeft: "3px" }}>
            Healthlane
            <Hearbeat />
          </h4>
        </div>
        {viewState.view === 'Home' &&
          <HomeView />
        }
        {viewState.view === 'User' &&
          <UserSettings />
        }
        {viewState.view === 'PersonsOfInterest' &&
          <UsersOfInterestPage />
        }
        <ToastContainer />
        <footer className="footer">
          <p>&copy; 2018</p>
        </footer>
      </div>
    );
  }
}