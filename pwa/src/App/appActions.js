import api from '../Server/api';
import loadSettings from '../Server/loadSettings';
import Promise from 'bluebird';
import { setEncounters } from '../Encounters/encountersActions';
import { setMessages } from '../Communications/communicationActions';
import { setUser } from '../Users/userActions';

export const SET_SETTINGS = 'SET_SETTINGS';

const setAppSettings = function setAppSettings(settings) {
  console.log(settings);
  return {
    type: SET_SETTINGS,
    payload: settings,
  }
}

const eventLoopTrick = function eventLoopTrick() {
  var q = new Promise(function(resolve) {
    window.setTimeout((x) => resolve(x), 10);
  });
  return q;
}

export function initializeApp() {
  return function initializeAppThunk(dispatch, getState) {
    var settingsLoader = loadSettings().then((response) => dispatch(setAppSettings(response.data)))
      .then(eventLoopTrick);
    settingsLoader.then((x) => {
      const server = api();
      server.fetchEncounters(getState().settings.userId)
        .then(response => dispatch(setEncounters(response.data)));
      server.fetchMessages()
        .then(response => dispatch(setMessages(response.data)));
      server.fetchUser(getState().settings.userId)
        .then(response => dispatch(setUser(response.data)));
    })
  }
}

