import React from 'react';
import PropTypes from 'prop-types';
import './timewalk.css';
import { setOffsetHours } from './timeMaster';
import now from './timeMaster';

export default class TimeWalk extends React.Component {
  static propTypes = {
    forceRefresh: PropTypes.func.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      offsetValue: 0
    }
  }
  walkHours(hours) {
    setOffsetHours(hours);
    this.setState({
      offsetValue: hours
    });
    this.props.forceRefresh();
  }
  render() {
    const {
      forceRefresh
    } = this.props;
    const {
      offsetValue
    } = this.state;
    return (
      <div className="timewalker__container">
        <div className="timewalker__control"
          onClick={() => this.walkHours(offsetValue - 6)}
        >
          <i className="fa fa-fast-backward" />
        </div>
        <div className="timewalker__control">
          <i className="fa fa-play" />
        </div>
        <div className="timewalker__control"
          onClick={() => this.walkHours(offsetValue + 6)}
        >
          <i className="fa fa-fast-forward" />
        </div>
        <div>
          {now().format('D.M H:mm')}
        </div>
      </div>
    )
  }
}