import { connect } from 'react-redux';
import UserSettingsView from './UserSettingsView.jsx';
import { saveUserContact } from './userActions';

const mapStateToProps = (state) => {
  const currentUser = state.user;
  const trustedUsers = currentUser.trustedUsers ? 
    state.users.filter(x => currentUser.trustedUsers.indexOf(x.userId) !== -1)
    : [];
  return {
    user: currentUser,
    trustedUsers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserContact: (userContact) => dispatch(saveUserContact(userContact))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSettingsView);
