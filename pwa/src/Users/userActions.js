import api from '../Server/api';

export const SET_USER = 'SET_USER';
export const CALL_IN_USER = 'CALL_IN_USER';

export const setUser = function(user) {
  return {
    type: SET_USER,
    payload: {
      user
    }
  }
}

export const saveUserContact = function saveUser(userContact) {
  return function saveUserContactThunk(dispatch, getState) {
    const server = api();
    dispatch(setUser(userContact));
    server.saveUser(userContact);
  }
}

export const callInUser = function (userId) {
  return {
    type: CALL_IN_USER,
    payload: {
      userId
    }
  }
}

export const userAtHospital = function (userId) {
  
}