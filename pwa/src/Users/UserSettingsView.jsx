import React from 'react';
import PropTypes from 'prop-types';
import ContactInformationForm from './ContactInformationForm.jsx';

export default class UserSettingsView extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired
  }
  render() {
    const {
      user,
      trustedUsers,
      saveUserContact
    } = this.props;
    return (
      <div className="col-xs-12">
        <h2>Personal information</h2>
        
        <h4><u>Contact information</u></h4>
        <ContactInformationForm user={user} {...user.contact} saveUserContact={saveUserContact}/>

        <h4><u>Trusted users</u></h4>
        {trustedUsers.length === 0 ?
          <div className="row">
          <h4>No-one has been set as trusted</h4>
          <div className="col-xs-12">
            <div className="alert alert-warning" role="alert">
              Others will be able to follow your progress at your appointments if you set them as trusted.
            </div>
          </div>
        </div>
          :
          <ul>
            {trustedUsers.map(tUser => {
              <li key={tUser.name} className="list-group-item">
                {tUser.name}
              </li>
            })}
          </ul>
        }
      </div>
    )
  }
}