const initialState = [{
  name: "Tauno Testaaja",
  userId: "testiparent",
  trustedUsers: [
    "testiuseri"
  ],
  encounters: [{
    startDate: "2018-06-17T09:00:00Z",
    endDate: "2018-06-17T09:30:00Z",
    description: "Outpatient visit",
    status: "inprogress",
  }, {
    startDate: "2018-06-17T11:30:00Z",
    endDate: "2018-06-17T12:30:00Z",
    description: "Outpatient visit",
    status: "planned",
  }, {
    startDate: "2018-06-17T13:00:00Z",
    endDate: "2018-06-17T13:30:00Z",
    description: "Outpatient visit",
    status: "planned",
  }],
}, {
  name: "Matti Meikäläinen",
  userId: "testichild",
  trustedUsers: [
    "testiuseri"
  ],
  encounters: [],
}]

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export default usersReducer;