import PropTypes from 'prop-types';
import { SET_USER } from './userActions';

const initialState = {
  userId: "testiuseri"
}

const shapeOfUser = PropTypes.shape({
  name: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  trustedUsers: PropTypes.array,
  status: PropTypes.string.isRequired,
  contact: PropTypes.shape({
    telecom: PropTypes.string,
    street: PropTypes.string,
    city: PropTypes.string,
    zipcode: PropTypes.string
  }),
});

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      if (action.payload.user.name) {
        return {
          userId: 'testiuseri',
          ...action.payload.user
        };
      }
      const newContact = {
        ...state.contact,
        ...action.payload.user
      }
      return {
        ...state,
        name: action.payload.user.firstName + " " + action.payload.user.lastName,
        contact: newContact
      }
    default:
      return state
  }
}

export default userReducer;