import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class UsersOfInterestPageView extends React.Component {
  static propTypes = {
    usersOfInterest: PropTypes.array
  }
  static defaultProps = {
    usersOfInterest: []
  }
  render() {
    const {
      usersOfInterest
    } = this.props;
    if (usersOfInterest.length === 0) {
      return (
        <div className="row">
          <h4>No-one to follow</h4>
          <div className="alert alert-warning" role="alert">
            Request a permission from the person whose treatment progress you want to follow
          </div>
        </div>
      )
    }
    return (
      <div className="row">
        <h4>Follow the progress of the treatment</h4>
        <div className="col-xs-12">
          {usersOfInterest.map(user => (
            <div key={user.userId} className="row well">
              <div className="col-xs-6">
                {user.name}
              </div>
              <div className="col-xs-6">
                {user.encounters.map(x => (
                  <div className="row">
                    {moment(x.startDate).format('HH:mm')} - {x.status}
                  </div>
                ))}
              </div>
              <div className="col-xs-12">
                {user.encounters.length === 0 ?
                  <div className="row">
                    <div className="progress">
                      <div className="progress-bar" role="progressbar" aria-valuenow="100"
                      aria-valuemin="0" aria-valuemax="100" style={{ width: '100%' }}>
                        No planned encounters today
                      </div>
                    </div>
                  </div>
                  :
                  <div className="row">
                    <div className="progress">
                      <div className="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="20"
                      aria-valuemin="0" aria-valuemax="100" style={{ width: '20%' }}>
                        In-progress 
                      </div>
                    </div>
                  </div>
                }
              </div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}