import { createSelector } from 'reselect';

export const getCurrentUser = state => state.user;
export const getOtherUsers = state => state.users;

export const getUsersOfInterest = createSelector(
  [getCurrentUser, getOtherUsers],
  (currentUser, users) =>
    users.filter(user => user.trustedUsers.indexOf(currentUser.userId) !== -1)
);

