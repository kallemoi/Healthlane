import React from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

const saveToast = () => {
  toast("Saved!", {
    type: toast.TYPE.SUCCESS
  });
};

export default class ContactInformationForm extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    telecom: PropTypes.string,
    street: PropTypes.string,
    city: PropTypes.string,
    zipcode: PropTypes.string,
    saveUserContact: PropTypes.func.isRequired
  }
  static defaultProps = {
    firstName: '',
    lastName: '',
    telecom: '',
    street: '',
    city: '',
    zipcode: ''
  }
  constructor(props) {
    super(props);
    this.state = {
      firstName: props.firstName,
      lastName: props.lastName,
      telecom: props.telecom,
      street: props.street,
      city: props.city,
      zipcode: props.zipcode
    }
  }
  saveForm() {
    const userContact = {
      identifier: this.props.user.userId,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      telecom: this.state.telecom,
      street: this.state.street,
      city: this.state.city,
      zipcode: this.state.zipcode,
    }
    this.props.saveUserContact(userContact);
  }
  render() {
    const {
      user,
    } = this.props;
    const {
      telecom,
      street,
      city,
      zipcode
    } = this.state;
    return (
      <div className="row">
        <div className="col-xs-12">
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="inputPhone" className="col-sm-2 control-label">Phone</label>
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  id="inputPhone"
                  placeholder="e.g. +358401234567"
                  value={telecom}
                  onChange={(e) => this.setState({ telecom: e.target.value })}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputAddress" className="col-sm-2 control-label">Address</label>
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  id="inputAddress"
                  placeholder=""
                  value={street}
                  onChange={(e) => this.setState({ street: e.target.value })}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="inputCity" className="col-sm-2 control-label">City</label>
              <div className="col-sm-4">
                <input
                  type="text"
                  className="form-control"
                  id="inputCity"
                  placeholder=""
                  value={city}
                  onChange={(e) => this.setState({ city: e.target.value })}
                />
              </div>
              <label htmlFor="inputZipcode" className="col-sm-2 control-label">Zip</label>
              <div className="col-sm-4">
                <input
                  type="text"
                  className="form-control"
                  id="inputZipcode"
                  placeholder=""
                  value={zipcode}
                  onChange={(e) => this.setState({ zipcode: e.target.value })}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button onClick={() => {
                  this.saveForm();
                  saveToast();
                  }}
                  type="button"
                  className="btn btn-default"
                >
                  Save
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}