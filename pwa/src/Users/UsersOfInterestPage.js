import { connect } from 'react-redux';
import UsersOfInterestPageView from './UsersOfInterestPageView';
import { getUsersOfInterest } from './userSelectors';

const mapStateToProps = (state) => {
  const usersOfInterest = getUsersOfInterest(state);
  return {
    usersOfInterest
  }
}

export default connect(mapStateToProps)(UsersOfInterestPageView)