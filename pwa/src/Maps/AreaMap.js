import React from "react"
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from "react-google-maps"

const AreaMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBwyHtRFUeydTlkDqsjJBAjVXe0VG3DogU&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      const DirectionsService = new window.google.maps.DirectionsService();

      DirectionsService.route({
        origin: new window.google.maps.LatLng(65.060246, 25.467953),
        destination: new window.google.maps.LatLng(65.007280, 25.518333),
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === window.google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  })
)((props) => (
  <GoogleMap
    defaultZoom={11}
    defaultCenter={{ lat: 65.027280, lng: 25.518333 }}
  >
    <Marker position={{ lat: 65.060246, lng: 25.467953 }} />
    <Marker position={{ lat: 65.007280, lng: 25.518333 }} />
    {props.directions && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
));

export default class AreaMap extends React.PureComponent {
  handleMarkerClick = () => {
    this.delayedShowMarker()
  }
  render() {
    return (
      <AreaMapComponent
        onMarkerClick={this.handleMarkerClick}
      />
    )
  }
}