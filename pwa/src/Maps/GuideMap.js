import { connect } from 'react-redux';
import GuideMapView from './GuideMapView.jsx';

const mapStateToProps = (state, ownProps) => ({
  mapName: ownProps.mapName
});

export default connect()(GuideMapView);
