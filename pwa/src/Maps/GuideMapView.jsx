import React from 'react';

export default class GuideMapView extends React.PureComponent {
  render() {
    const { mapName } = this.props;
    console.log(mapName);
    return (
      <div className="row">
        <div className="col-xs-12">
          <img src={mapName} style={{ maxWidth: "90%" }}/>
        </div>
      </div>
    )
  }
}