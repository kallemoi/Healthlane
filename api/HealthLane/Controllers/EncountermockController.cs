﻿

using System.Collections.Generic;
using HealthLane.Models;

namespace HealthLane.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]")]
    [ApiController]

    public class EncountermockController : ControllerBase
    {   
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public List<EncounterViewModel> Get(int id)
        {
            var list = new List<EncounterViewModel>
            {
                new EncounterViewModel
                {
                    description = "Outpatient visit, runny nose",
                    startDate = "2018-01-12T07:00:00Z",
                    endDate = "2018-01-12T07:15:00Z",
                    status = "finished"
                },
                new EncounterViewModel
                {
                    description = "Inpatient visit, influenza",
                    startDate = "2018-01-15T16:00:00Z",
                    endDate = "2018-02-01T12:30:00Z",
                    status = "finished"
                },
                new EncounterViewModel
                {
                    description = "Dentist",
                    startDate = "2018-01-20T10:30:00Z",
                    endDate = "2018-01-20T11:30:00Z",
                    status = "cancelled"
                },
                new EncounterViewModel
                {
                    description = "Outpatient visit, broken leg",
                    startDate = "2018-05-30T12:00:00Z",
                    endDate = "2018-05-30T12:30:00Z",
                    status = "finished"
                },
                new EncounterViewModel
                {
                    description = "Laboratory, fasting required",
                    startDate = "2018-06-05T05:00:00Z",
                    endDate = "2018-06-05T05:15:00Z",
                    status = "finished"
                },
                new EncounterViewModel
                {
                    description = "Outpatient visit, serious allergy",
                    startDate = "2018-06-18T15:00:00Z",
                    status = "planned",
                },
                new EncounterViewModel
                {
                    description = "Outpatient visit, dentist",
                    startDate = "2018-06-21T15:00:00Z",
                    status = "planned",
                },
            };
            return list;
        }
    }
}
