﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthLane.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthLane.Controllers
{
    [Route("[controller]")]
    public class MessageController : Controller
    {
        [HttpGet]
        public IList<Message> Get()
        {
            return new List<Message>{ 
                new Message{
                    id = 1,
                    sender = "testiuseri",
                    text = "Hi, I'm still feeling ill, what should I do?",
                    timestamp = "2018-06-15T10:00:00Z"
                }, 
                new Message{
                    id = 2,
                    sender = "testidoctor",
                    text = "Hello! I promise to write you a prescription",
                    timestamp = "2018-06-15T10:30:00Z"
                },
                new Message{
                    id = 3,
                    sender = "testiuseri",
                    text = "You promised me a prescription, where is it?",
                    timestamp = "2018-06-18T08:00:00Z"
                } };
        }
    }
}
