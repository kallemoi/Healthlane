﻿namespace HealthLane.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Hl7.Fhir.Model;
    using Hl7.Fhir.Rest;
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]")]
    [ApiController]

    public class PatientController : ControllerBase
    {
        // Static variables
        private static string baseUri = "http://85.23.127.238:57773/csp/healthshare/fhirserver/fhir";
        private static string requestPatientUri = "/Patient/";
        //private static string requestAll = "/$everything";
        private static readonly FhirClient fhirClient = new FhirClient(baseUri);

        /// <summary>
        /// Get patient patient by patient identifier
        /// </summary>
        /// <returns>Patient</returns>
        /// <param name="id">Identifier.</param>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Models.UIPatient>> Get(int? id)
        {
            if (id.HasValue && id.Value != 0)
            {
                var patient = await GetPatient(id.Value);
                if (patient != null)
                {
                    return Ok(patient);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Update Patient initial data
        /// </summary>
        /// <param name="model">UIPatient.</param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public Task<Patient> Post(Models.UIPatient model)
        {
            /*Models.UIPatient model = new Models.UIPatient();
            model.identifier = "220";
            model.firstName = "Marla";
            model.lastName = "Gonzales";
            model.street = "40 Great Plain Ave";
            model.zipcode = "60280";
            model.city = "Chicago";
            model.telecom = "123456";*/
            // TODO update data
            var result = UpdatePatientInitialData(model);

            return result;
        }

        private async Task<Patient> UpdatePatientInitialData(Models.UIPatient patientData)
        {
            if (fhirClient == null || patientData == null)
            {
                return null;
            }

            fhirClient.ReturnFullResource = true;

            var patient = await fhirClient.ReadAsync<Patient>(requestPatientUri + patientData.identifier);

            if (patient != null)
            {
                // We do not update names
                //patient.Name.Add(new HumanName().WithGiven(patientData.firstName).AndFamily(patientData.lastName));

                if (patient.Address.Count > 0)
                {
                    var tempAddress = patient.Address.Where(tn => tn.TypeName == "Address").FirstOrDefault();

                    tempAddress.LineElement.FirstOrDefault().Value = patientData.street;
                    tempAddress.PostalCode = patientData.zipcode;
                    tempAddress.City = patientData.city;
                }
                var tmpTelecom = patient.Telecom.Where(tc => !string.IsNullOrEmpty(tc.Value)).FirstOrDefault();
                if (tmpTelecom == null)
                {
                    patient.Telecom.Add(new ContactPoint(ContactPoint.ContactPointSystem.Phone, ContactPoint.ContactPointUse.Mobile, patientData.telecom));
                }
                else
                {
                    tmpTelecom.Value = patientData.telecom;
                }

                var result = await fhirClient.UpdateAsync(patient);

                return result != null ? result : null;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the encounter.
        /// </summary>
        /// <returns>The encounter.</returns>
        /// <param name="id">Identifier.</param>
        private async Task<Models.UIPatient> GetPatient(int id)
        {
            if (fhirClient == null)
            {
                return null;
            }

            fhirClient.Timeout = 120000;

            var patient = await fhirClient.ReadAsync<Patient>(requestPatientUri + id.ToString());

            if (patient != null)
            {
                Models.UIPatient uiPatient = new Models.UIPatient();
                uiPatient.identifier = id.ToString();
                uiPatient.firstName = patient.Name.Where(hn => !string.IsNullOrEmpty(hn.Given.FirstOrDefault())).Select(t => t.Given.FirstOrDefault()).FirstOrDefault();
                uiPatient.lastName = patient.Name.Where(hn => !string.IsNullOrEmpty(hn.Family.FirstOrDefault())).Select(t => t.Family.FirstOrDefault()).FirstOrDefault();

                if (patient.Address.Count > 0)
                {
                    var tempAddress = patient.Address.Where(tn => tn.TypeName == "Address").FirstOrDefault();

                    if (tempAddress != null)
                    {
                        uiPatient.street = tempAddress.Line.FirstOrDefault();
                        uiPatient.zipcode = tempAddress.PostalCode;
                        uiPatient.city = tempAddress.City;
                    }
                }
                uiPatient.telecom = patient.Telecom.Where(tc => !string.IsNullOrEmpty(tc.Value)).Select(v => v.Value).FirstOrDefault();
                return uiPatient;
            }
            else
            {
                return null;
            }
        }
    }
}
