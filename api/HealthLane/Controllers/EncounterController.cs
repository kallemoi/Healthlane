﻿namespace HealthLane.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Hl7.Fhir.Model;
    using Hl7.Fhir.Rest;
    using Microsoft.AspNetCore.Mvc;

    [Route("[controller]")]
    [ApiController]

    public class EncounterController : ControllerBase
    {
        // Static variables
        private static string baseUri = "http://85.23.127.238:57773/csp/healthshare/fhirserver/fhir";
        private static string requestEncounterUri = "/Encounter/";
        private static string requestPatientUri = "/Patient/";
        private static readonly FhirClient fhirClient = new FhirClient(baseUri);

        /// <summary>
        /// Get patient encounters by patient identifier
        /// </summary>
        /// <returns>Encounter</returns>
        /// <param name="id">Identifier.</param>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<Encounter>>> Get(int? id)
        {
            if (id.HasValue && id.Value != 0)
            {
                var encounter = await GetEncounter(id.Value);
                if (encounter != null)
                {
                    return Ok(encounter);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Gets the encounter.
        /// </summary>
        /// <returns>The encounter.</returns>
        /// <param name="id">Identifier.</param>
        private async Task<List<Encounter>> GetEncounter(int id)
        {
            if (fhirClient == null)
            {
                return null;
            }

            fhirClient.Timeout = 120000;

            string uri = requestPatientUri + id.ToString() + requestEncounterUri ;
            Console.WriteLine(uri);
            SearchParams sParams = new SearchParams();
            sParams.Where("Patient.identifier=" + id.ToString());

            var encounter = await fhirClient.SearchAsync<Encounter>(sParams);

            List<Encounter> encounterList = new List<Encounter>();

            foreach (var e in encounter.Entry)
            {
                encounterList.Add((Encounter)e.Resource);
            }

            if (encounterList != null)
            {
                return encounterList;
            }
            else
            {
                return null;
            }
        }
    }
}
