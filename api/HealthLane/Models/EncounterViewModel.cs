﻿namespace HealthLane.Models
{
    public class EncounterViewModel
    {
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string description { get; set; }
        public string status { get; set; }
    }
}