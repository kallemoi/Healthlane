﻿using System;
namespace HealthLane.Models
{
    public class Message
    {
        public int id { get; set; }
        public String sender { get; set; }
        public String timestamp { get; set; }
        public String text { get; set; }
    }
}