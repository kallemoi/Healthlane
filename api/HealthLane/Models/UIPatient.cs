﻿namespace HealthLane.Models
{
    /// <summary>
    /// Patient class for UI.
    /// </summary>
    public class UIPatient
    {
        public string identifier;
        public string firstName;
        public string lastName;
        public string street;
        public string zipcode;
        public string city;
        public string telecom;
    }
}